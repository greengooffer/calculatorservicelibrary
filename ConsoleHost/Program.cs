﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleHost
{
    class Program
    {
        static void DispalyHostInfo (ServiceHost host)
        {
            Console.WriteLine();
            Console.WriteLine("*** *** *** *** *** *** *** *** *** ***");
            foreach (System.ServiceModel.Description.ServiceEndpoint s in host.Description.Endpoints)
            {
                Console.WriteLine("Addres: {0}", s.Address);
                Console.WriteLine("Binding: {0}", s.Binding.Name);
                Console.WriteLine("Contract: {0}", s.Contract.Name);
                Console.WriteLine();
            }
            Console.WriteLine("*** *** *** *** *** *** *** *** *** ***");
            Console.WriteLine();
        }

        static void Main(string[] args)
        {
            //Console.BackgroundColor = ConsoleColor.Blue;
            //Console.ForegroundColor = ConsoleColor.White;
            var host = new ServiceHost(typeof(CalculatorServiceLibrary.CalculatorService));
            host.Open();
            DispalyHostInfo(host);
            Console.WriteLine("The service is ready");
            Console.WriteLine("Press Enter key to termonate service");
            Console.ReadLine();
            host.Close();
            Console.ReadLine();
        }
    }
}
