﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;

namespace CalculatorServiceLibrary
{
    /// <summary>
    /// 
    /// </summary>
    [ServiceContract]
    public interface ICalculatorService
    {
        [OperationContract]
        [WebGet]
        int Add(int a, int b);

        [OperationContract]
        [WebGet]
        [FaultContract(typeof(System.DivideByZeroException))]
        int Div(int a, int b);

        [OperationContract]
        [WebGet]
        [FaultContract(typeof(OverflowException))]
        int Mul(int a, int b);
    }
    [DataContract]
    public class Fault
    {
        public const string Action = "Error";
        [DataMember]
        public string Message
        {
            get;
            set;
        }
    }
}
