﻿using System;
using System.ServiceModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculatorServiceLibrary
{
    [GlobalErrorBehaviourAttribute(typeof(GlobalErrorHandler))]
    public class CalculatorService : ICalculatorService
    {
        public int Add(int a, int b)
        {
            return a + b;
        }
        public int Div(int a, int b)
        {
            return a / b;
        }
        public int Mul(int a, int b)
        {
            checked
            {
                try
                {
                    return a * b;
                }
                catch (OverflowException ex)
                {
                    throw new FaultException<OverflowException>(ex);
                }
            }           
        }
    }
}
