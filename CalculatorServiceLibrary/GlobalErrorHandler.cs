﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.Serialization.Json;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Dispatcher;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;

namespace CalculatorServiceLibrary
{
    public class GlobalErrorHandler : IErrorHandler
    {
        //XML version of ProvideFault
        //
        //public void ProvideFault(Exception error, MessageVersion version, ref Message fault)
        //{
        //    var newEx = new FaultException(string.Format("Exception caught at Application Error Handler{0}. Method: {1}{2}. Message:{3}",
        //        Environment.NewLine, error.TargetSite.Name, Environment.NewLine, error.Message));
        //    var msgFault = newEx.CreateMessageFault();
        //    fault = Message.CreateMessage(version, msgFault, newEx.Action);
        //}

        public void ProvideFault(Exception error, MessageVersion version, ref Message message)
        {
            var fault = new CalculatorServiceLibrary.Fault
            {
                Message = error.Message
            };
            toJson(fault, ref message);
        }
        void toJson(CalculatorServiceLibrary.Fault fault, ref Message message)
        {
            message = WebOperationContext.Current.CreateJsonResponse<Fault>((Fault)fault, new DataContractJsonSerializer(typeof(Fault)));
            var response = WebOperationContext.Current.OutgoingResponse;
            response.StatusCode = System.Net.HttpStatusCode.InternalServerError;
            response.StatusDescription = "Custom Fault";
        }
        public bool HandleError(Exception error)
        {   
            return true;
        }
    }
}
